# Requisitos

* Cadastrar nova pesquisa
* Listar pesquisas criadas
* Mostrar status da pesquisa conjuntamente com a lista de urls encontradas

# Possíveis Erros e ações a serem tomadas

* Palavras de inspeção não válidas (4-30 caracteres): prevenção na UI.
* Serviço indisponível para cadastro / outro erro crítico: mensagem de erro pedindo para repetir a pesquisa e caso continuar entrar em contato.
* Problema com serviço de consulta: mensagem de erro solicitando para excluir pesquisa atual e refazer a consulta.

# Experiência de usuário proposta

* Bloquear botão de submissão em caso de solicitação não válida.
* Botão de refresh na pesquisa caso não esteja 'done'.
* Salvar pesquisas na storage do navegador.
* Excluir pesquisas realizadas.
* Accordion nos resultados da pesquisa, expandindo com 5 items e possibilidade de expandir todos.
* Layout minimamente responsivo com breakpoints para telas menores.