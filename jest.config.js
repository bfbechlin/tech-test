module.exports = {
  preset: 'ts-jest',
  roots: ['<rootDir>/src'],
  clearMocks: true,
  transform: {
    '^.+\\.tsx?$': 'ts-jest'
  },
  setupFiles: ['@testing-library/react/dont-cleanup-after-each'],
  testRegex: 'src\/.*\.(test|spec)\.(ts|tsx)$',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  collectCoverage: true,
  collectCoverageFrom: ['src/**/*.{ts,tsx}'],
  coveragePathIgnorePatterns: [
    'node_modules',
    '<rootDir>/src/index.tsx',
    '<rootDir>/src/__tests__/'
  ]
};