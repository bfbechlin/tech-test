import React from "react";
import { render, cleanup } from "../__tests__";

import { Button, Input } from "./Commom";

describe("Commom", () => {
  afterEach(cleanup);

  describe("Button", () => {
    it("should display custom button", () => {
      const { container } = render(<Button> TEST </Button>);
      expect(
        container.getElementsByTagNameNS(
          "http://www.w3.org/1999/xhtml",
          "button"
        )
      ).toHaveLength(1);
    });
  });

  describe("Input", () => {
    it("should display the alert message", () => {
      const { container } = render(<Input />);
      expect(
        container.getElementsByTagNameNS(
          "http://www.w3.org/1999/xhtml",
          "input"
        )
      ).toHaveLength(1);
    });
  });
});
