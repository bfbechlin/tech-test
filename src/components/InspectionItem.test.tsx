import React from "react";
import { render, fireEvent, cleanup, theme } from "../__tests__";
import {
  active_inspection,
  done_inspection,
  four_urls_inspection,
  eight_urls_inspection,
} from "../__tests__/inspection.mock";
import InspectionItem from "./InspectionItem";

describe("InspectionItem", () => {
  afterEach(cleanup);

  it("should display the inspection id", () => {
    const { queryByText } = render(
      <InspectionItem
        id={active_inspection.id}
        status={active_inspection.status}
        urls={active_inspection.urls}
        onRefresh={() => {}}
        onDelete={() => {}}
      />
    );
    expect(queryByText(active_inspection.id)).toBeVisible();
  });

  it("should display the inspection 'ATIVO' status", () => {
    const { queryByText } = render(
      <InspectionItem
        id={active_inspection.id}
        status={active_inspection.status}
        urls={active_inspection.urls}
        onRefresh={() => {}}
        onDelete={() => {}}
      />
    );
    expect(queryByText("ativo")).toBeVisible();
  });

  it("should display the inspection 'COMPLETO' status", () => {
    const { queryByText } = render(
      <InspectionItem
        id={done_inspection.id}
        status={done_inspection.status}
        urls={done_inspection.urls}
        onRefresh={() => {}}
        onDelete={() => {}}
      />
    );
    expect(queryByText("completo")).toBeVisible();
  });

  it("should call the onDelete callback once the delete button is clicked", () => {
    const onDelete = jest.fn();

    const { getByText } = render(
      <InspectionItem
        id={active_inspection.id}
        status={active_inspection.status}
        urls={active_inspection.urls}
        onRefresh={() => {}}
        onDelete={onDelete}
      />
    );
    const button = getByText("Deletar") as HTMLButtonElement;

    fireEvent.click(button);
    expect(onDelete).toHaveBeenCalled();
  });

  it("should call the onRefresh callback if the status is active once the refresh button is clicked", () => {
    const onRefresh = jest.fn();

    const { getByText } = render(
      <InspectionItem
        id={active_inspection.id}
        status={active_inspection.status}
        urls={active_inspection.urls}
        onRefresh={onRefresh}
        onDelete={() => {}}
      />
    );
    const button = getByText("Recarregar") as HTMLButtonElement;

    fireEvent.click(button);
    expect(onRefresh).toHaveBeenCalled();
  });

  it("should not call the onRefresh callback if the status is done once the refresh button is clicked", () => {
    const onRefresh = jest.fn();

    const { getByText } = render(
      <InspectionItem
        id={done_inspection.id}
        status={done_inspection.status}
        urls={done_inspection.urls}
        onRefresh={onRefresh}
        onDelete={() => {}}
      />
    );
    const button = getByText("Recarregar") as HTMLButtonElement;

    fireEvent.click(button);
    expect(onRefresh).toHaveBeenCalledTimes(0);
  });

  it("should able the refresh button when the inspection status is active", () => {
    const { getByText } = render(
      <InspectionItem
        id={active_inspection.id}
        status={active_inspection.status}
        urls={active_inspection.urls}
        onRefresh={() => {}}
        onDelete={() => {}}
      />
    );
    const button = getByText("Recarregar") as HTMLButtonElement;

    expect(button.attributes["disabled"]).toBeFalsy();
    expect(button).toHaveStyle(`background-color: ${theme.colors.green[500]}`);
  });

  it("should disable the refresh button when the inspection status is done", () => {
    const { getByText } = render(
      <InspectionItem
        id={done_inspection.id}
        status={done_inspection.status}
        urls={done_inspection.urls}
        onRefresh={() => {}}
        onDelete={() => {}}
      />
    );
    const button = getByText("Recarregar") as HTMLButtonElement;

    expect(button.attributes["disabled"]).toBeTruthy();
    expect(button).toHaveStyle("background-color: grey");
  });

  it("should not display urls because the accordion is closed when mounted", () => {
    const { queryByText } = render(
      <InspectionItem
        id={active_inspection.id}
        status={active_inspection.status}
        urls={active_inspection.urls}
        onRefresh={() => {}}
        onDelete={() => {}}
      />
    );

    active_inspection.urls.forEach((link) =>
      expect(queryByText(link)).toBeNull()
    );
    expect(queryByText("Ver mais")).toBeNull();
    expect(queryByText("Ver menos")).toBeNull();
  });

  it("should display all urls and no nav buttons since user has clicked in the ID and there are less than 5 urls", () => {
    const { queryByText, getByText } = render(
      <InspectionItem
        id={four_urls_inspection.id}
        status={four_urls_inspection.status}
        urls={four_urls_inspection.urls}
        onRefresh={() => {}}
        onDelete={() => {}}
      />
    );
    fireEvent.click(getByText(four_urls_inspection.id));

    four_urls_inspection.urls.forEach((link) =>
      expect(queryByText(link)).toBeVisible()
    );
    expect(queryByText("Ver mais")).toBeNull();
    expect(queryByText("Ver menos")).toBeNull();
  });

  it("should display only the first 5 urls and Show More nav button since user has clicked in the ID and there are more than 5 urls", () => {
    const { queryByText, getByText } = render(
      <InspectionItem
        id={eight_urls_inspection.id}
        status={eight_urls_inspection.status}
        urls={eight_urls_inspection.urls}
        onRefresh={() => {}}
        onDelete={() => {}}
      />
    );
    fireEvent.click(getByText(eight_urls_inspection.id));

    eight_urls_inspection.urls
      .filter((_, i) => i < 5)
      .forEach((link) => expect(queryByText(link)).toBeVisible());
    eight_urls_inspection.urls
      .filter((_, i) => i >= 5)
      .forEach((link) => expect(queryByText(link)).toBeNull());
    expect(queryByText("Ver mais")).toBeVisible();
    expect(queryByText("Ver menos")).toBeNull();
  });

  it("should close the accordion once the user click twice in the ID", () => {
    const { queryByText, getByText } = render(
      <InspectionItem
        id={eight_urls_inspection.id}
        status={eight_urls_inspection.status}
        urls={eight_urls_inspection.urls}
        onRefresh={() => {}}
        onDelete={() => {}}
      />
    );
    fireEvent.click(getByText(eight_urls_inspection.id));
    fireEvent.click(getByText(eight_urls_inspection.id));

    eight_urls_inspection.urls
      .filter((_, i) => i < 5)
      .forEach((link) => expect(queryByText(link)).toBeNull());
    expect(queryByText("Ver mais")).toBeNull();
    expect(queryByText("Ver menos")).toBeNull();
  });

  it("should show all urls when the user clicks in the Show More button", () => {
    const { queryByText, getByText } = render(
      <InspectionItem
        id={eight_urls_inspection.id}
        status={eight_urls_inspection.status}
        urls={eight_urls_inspection.urls}
        onRefresh={() => {}}
        onDelete={() => {}}
      />
    );
    fireEvent.click(getByText(eight_urls_inspection.id));
    fireEvent.click(getByText("Ver mais"));

    eight_urls_inspection.urls.forEach((link) =>
      expect(queryByText(link)).toBeVisible()
    );
    expect(queryByText("Ver mais")).toBeNull();
    expect(queryByText("Ver menos")).toBeVisible();
  });

  it("should show the first 5 urls again when the user click in show more and than in show less", () => {
    const { queryByText, getByText } = render(
      <InspectionItem
        id={eight_urls_inspection.id}
        status={eight_urls_inspection.status}
        urls={eight_urls_inspection.urls}
        onRefresh={() => {}}
        onDelete={() => {}}
      />
    );
    fireEvent.click(getByText(eight_urls_inspection.id));
    fireEvent.click(getByText("Ver mais"));

    eight_urls_inspection.urls.forEach((link) =>
      expect(queryByText(link)).toBeVisible()
    );
    expect(queryByText("Ver mais")).toBeNull();
    expect(queryByText("Ver menos")).toBeVisible();

    fireEvent.click(getByText("Ver menos"));

    eight_urls_inspection.urls
      .filter((_, i) => i < 5)
      .forEach((link) => expect(queryByText(link)).toBeVisible());
    eight_urls_inspection.urls
      .filter((_, i) => i >= 5)
      .forEach((link) => expect(queryByText(link)).toBeNull());
    expect(queryByText("Ver mais")).toBeVisible();
    expect(queryByText("Ver menos")).toBeNull();
  });
});
