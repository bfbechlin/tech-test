import React from "react";
import { render, fireEvent, cleanup, theme } from "../__tests__";

import AlertCard from "./AlertCard";
import { successAlert, errorAlert } from "../__tests__/alert.mock";

describe("AlertCard", () => {
  afterEach(cleanup);

  it("should display the alert message", () => {
    const { queryByText } = render(
      <AlertCard
        timestamp={successAlert.timestamp}
        message={successAlert.message}
        type={successAlert.type}
        onClose={() => {}}
      />
    );
    expect(queryByText(successAlert.message)).toBeVisible();
  });

  it("should call the onClose callback once the close button is clicked passing the timestamp", () => {
    const onClose = jest.fn();

    const { getByTitle } = render(
      <AlertCard
        timestamp={successAlert.timestamp}
        message={successAlert.message}
        type={successAlert.type}
        onClose={onClose}
      />
    );
    const closeBtt = getByTitle("Fechar");
    fireEvent.click(closeBtt);
    expect(onClose).toHaveBeenCalledWith(successAlert.timestamp);
  });

  it("should be green when is a success alert", () => {
    const { container } = render(
      <AlertCard
        timestamp={successAlert.timestamp}
        message={successAlert.message}
        type={successAlert.type}
        onClose={() => {}}
      />
    );
    expect(container.firstChild).toHaveStyle(
      `background-color: ${theme.colors.green[50]}`
    );
    expect(container.firstChild).toHaveStyle(
      `border-color: ${theme.spacing[1]} solid ${theme.colors.green[500]}`
    );
  });

  it("should be red when is a error alert", () => {
    const { container } = render(
      <AlertCard
        timestamp={errorAlert.timestamp}
        message={errorAlert.message}
        type={errorAlert.type}
        onClose={() => {}}
      />
    );
    expect(container.firstChild).toHaveStyle(
      `background-color: ${theme.colors.red[50]}`
    );
    expect(container.firstChild).toHaveStyle(
      `border-color: ${theme.spacing[1]} solid ${theme.colors.red[500]}`
    );
  });
});
