import React, { useState } from "react";
import styled from "styled-components";

import { Button } from "./Commom";
import { Inspection, InspectionStatus } from "./../store/inspection/types";

const Container = styled.div`
  display: flex;
  flex-direction: column;

  width: 100%;
  margin: ${(props) => props.theme.spacing[2]};
  padding: ${(props) => props.theme.spacing[2]};
  border-radius: ${(props) => props.theme.radius.md};
  box-shadow: ${(props) => props.theme.shadow.lg};

  background-color: white;
`;

const Header = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  padding: ${(props) => props.theme.spacing[2]};
`;

const URLContainer = styled.div`
  width: 100%;
  padding-top: ${(props) => props.theme.spacing[2]};
`;

const URLItem = styled.div`
  width: 100%;
  border-top: 1px solid rgba(0, 0, 0, 0.15);
  padding: ${(props) => props.theme.spacing[1]} 0;
`;

const NavButton = styled(Button)`
  width: 100%;
  background-color: ${(props) => props.theme.colors.green[500]};
`;

const IDField = styled.span`
  cursor: pointer;
  padding: 0 ${(props) => props.theme.spacing[2]};
  flex-grow: 1;
`;

const StatusField = styled.span`
  padding: 0 ${(props) => props.theme.spacing[2]};
  width: 60px;
`;

const CountField = styled.span`
  text-align: center;
  width: 50px;
`;

const RefreshButton = styled(Button)<{ disabled: boolean }>`
  ${(props) => props.disabled && "cursor: initial;"}
  margin: 0 ${(props) => props.theme.spacing[2]};

  background-color: ${(props) =>
    props.disabled ? "grey" : props.theme.colors.green[500]};

  &:hover {
    background-color: ${(props) =>
      props.disabled ? "grey" : props.theme.colors.green[600]};
  }
`;

const DeleteButton = styled(Button)`
  background-color: ${(props) => props.theme.colors.red[500]};

  &:hover {
    background-color: ${(props) => props.theme.colors.red[600]};
  }
`;

export enum AccordionState {
  CLOSED,
  PREVIEW,
  OPENED,
}

export const previewCloseLogic = (state: AccordionState) => {
  if (state === AccordionState.OPENED || state === AccordionState.PREVIEW) {
    return AccordionState.CLOSED;
  } else {
    return AccordionState.PREVIEW;
  }
};

export const statusToLabel = (status: InspectionStatus) => {
  switch (status) {
    case InspectionStatus.ACTIVE:
      return "ativo";
    case InspectionStatus.DONE:
      return "completo";
  }
};

export interface URLAreaProps {
  urls: string[];
  preview: boolean;
  onChangePreview: () => void;
}

export const URLArea: React.FC<URLAreaProps> = ({
  urls,
  preview,
  onChangePreview,
}) => {
  const URLS = preview ? urls.filter((_, i) => i < 5) : urls;
  return (
    <URLContainer>
      {URLS.map((url, i) => (
        <URLItem key={i}>
          <a href={url}>{url}</a>
        </URLItem>
      ))}
      {urls.length > 5 && (
        <NavButton onClick={onChangePreview}>
          {preview ? "Ver mais" : "Ver menos"}
        </NavButton>
      )}
    </URLContainer>
  );
};

export interface InspectionItemProps extends Inspection {
  onRefresh: () => void;
  onDelete: () => void;
}

export const InspectionItem: React.FC<InspectionItemProps> = ({
  id,
  status,
  urls,
  onRefresh,
  onDelete,
}) => {
  const [state, changeState] = useState(AccordionState.CLOSED);

  return (
    <Container>
      <Header>
        <IDField
          onClick={() => {
            changeState(previewCloseLogic(state));
          }}
        >
          {id}
        </IDField>
        <StatusField color={status}> {statusToLabel(status)} </StatusField>
        <CountField> ({urls.length}) </CountField>
        <RefreshButton
          onClick={onRefresh}
          disabled={status === InspectionStatus.DONE}
        >
          Recarregar
        </RefreshButton>
        <DeleteButton onClick={onDelete}> Deletar </DeleteButton>
      </Header>
      {state !== AccordionState.CLOSED && urls.length > 0 && (
        <URLArea
          urls={urls}
          preview={state === AccordionState.PREVIEW}
          onChangePreview={() =>
            changeState(
              state === AccordionState.PREVIEW
                ? AccordionState.OPENED
                : AccordionState.PREVIEW
            )
          }
        />
      )}
    </Container>
  );
};

export default InspectionItem;
