import React from "react";
import styled from "styled-components";
import { Alert, AlertType } from "../store/alert/types";
import { CloseSVG } from "./Commom";

const Container = styled.div<{ type: AlertType }>`
  display: flex;
  justify-content: space-between;
  align-items: center;

  width: 100%;
  margin: ${(props) => props.theme.spacing[2]};
  padding: ${(props) => props.theme.spacing[4]};
  border-radius: ${(props) => props.theme.radius.md};
  box-shadow: ${(props) => props.theme.shadow.md};
  border-top: ${(props) => props.theme.spacing[1]} solid
    ${(props) =>
      props.type === AlertType.success
        ? props.theme.colors.green[500]
        : props.theme.colors.red[500]};
  background-color: ${(props) =>
    props.type === AlertType.success
      ? props.theme.colors.green[50]
      : props.theme.colors.red[50]};
`;

const Content = styled.span``;

const CloseButton = styled.svg`
  width: ${(props) => props.theme.spacing[6]};
  cursor: pointer;
`;

export interface AlertProps extends Alert {
  onClose: (number) => void;
}

export const AlertCard: React.FC<AlertProps> = ({
  type,
  timestamp,
  message,
  onClose,
}) => {
  return (
    <Container type={type}>
      <Content> {message} </Content>
      <CloseButton
        onClick={() => onClose(timestamp)}
        role="button"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 20 20"
      >
        <title>Fechar</title>
        {CloseSVG}
      </CloseButton>
    </Container>
  );
};

export default AlertCard;
