import React from "react";
import { render, fireEvent, cleanup, theme } from "../__tests__";

import SearchBox from "./SearchBox";

describe("SearchBox", () => {
  afterEach(cleanup);

  it("should store the typed values in the input", () => {
    const onSubmit = jest.fn();
    const typed = "TEST";

    const { getByPlaceholderText } = render(<SearchBox onSubmit={onSubmit} />);
    const input = getByPlaceholderText("Nova Busca") as HTMLInputElement;

    fireEvent.change(input, { target: { value: typed } });
    expect(input.value).toBe(typed);
  });

  it("should disable the submit button and show message when typed less than 4 characters", () => {
    const onSubmit = jest.fn();
    const typed = "TES";

    const { getByPlaceholderText, getByText, queryByText } = render(
      <SearchBox onSubmit={onSubmit} />
    );
    const input = getByPlaceholderText("Nova Busca") as HTMLInputElement;
    const button = getByText("Buscar") as HTMLButtonElement;

    fireEvent.change(input, { target: { value: typed } });

    expect(button.attributes["disabled"]).toBeTruthy();
    expect(button).toHaveStyle("background-color: grey");

    fireEvent.click(button);

    expect(onSubmit).toHaveBeenCalledTimes(0);
    expect(queryByText(/pesquisa deve conter/)).toBeVisible();
  });

  it("should disable the submit button and show message when typed is more than 32 characters", () => {
    const onSubmit = jest.fn();
    const typed =
      "TESSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSST";

    const { getByPlaceholderText, getByText, queryByText } = render(
      <SearchBox onSubmit={onSubmit} />
    );
    const input = getByPlaceholderText("Nova Busca") as HTMLInputElement;
    const button = getByText("Buscar") as HTMLButtonElement;

    fireEvent.change(input, { target: { value: typed } });

    expect(button.attributes["disabled"]).toBeTruthy();
    expect(button).toHaveStyle("background-color: grey");

    fireEvent.click(button);

    expect(onSubmit).toHaveBeenCalledTimes(0);
    expect(queryByText(/pesquisa deve conter/)).toBeVisible();
  });

  it("should call the onSubmit callback with typed keyword and no message, when valid, once the button is clicked", () => {
    const onSubmit = jest.fn();
    const typed = "TEST";

    const { getByPlaceholderText, getByText, queryByText } = render(
      <SearchBox onSubmit={onSubmit} />
    );
    const input = getByPlaceholderText("Nova Busca") as HTMLInputElement;
    const button = getByText("Buscar") as HTMLButtonElement;

    fireEvent.change(input, { target: { value: typed } });

    expect(button.attributes["disabled"]).toBeFalsy();
    expect(button).toHaveStyle(`background-color: ${theme.colors.green[500]}`);

    expect(queryByText(/pesquisa deve conter/)).toBeNull();

    fireEvent.click(button);

    expect(onSubmit).toHaveBeenCalledWith(typed);
  });
});
