import React, { useState, useEffect } from "react";
import styled from "styled-components";

import { Button } from "./Commom";

const Container = styled.form`
  display: flex;
  flex-direction: column;
  width: 100%;
  align-items: center;
`;

const Input = styled.input`
  border-radius: ${(props) => props.theme.radius.lg};
  padding: ${(props) => props.theme.spacing[2]};
  margin: ${(props) => props.theme.spacing[2]};

  font-size: 1rem;
  line-height: 1.5rem;
  width: 275px;

  &:focus {
    outline: none;
    box-shadow: ${(props) => props.theme.shadow.sm};
  }
`;

const SearchButton = styled(Button)<{ disabled: boolean }>`
  margin: ${(props) => props.theme.spacing[2]};
  ${(props) => props.disabled && "cursor: initial;"}

  background-color: ${(props) =>
    props.disabled ? "grey" : props.theme.colors.green[500]};

  &:hover {
    background-color: ${(props) =>
      props.disabled ? "grey" : props.theme.colors.green[600]};
  }
`;

const SearchArea = styled.div`
  margin: 0 -${(props) => props.theme.spacing[2]};
`;

const AlertArea = styled.div`
  height: ${(props) => props.theme.spacing[8]};
`;

export interface SearchBoxProps {
  onSubmit: (keyword: string) => void;
}

export const SearchBox: React.FC<SearchBoxProps> = ({ onSubmit }) => {
  const [text, setText] = useState("");
  const [isAble, setIsAble] = useState(false);

  useEffect(() => {
    if (text.length >= 4 && text.length <= 32) {
      setIsAble(true);
    } else {
      setIsAble(false);
    }
  }, [text]);

  return (
    <Container
      onSubmit={(evt) => {
        evt.preventDefault();
        onSubmit(text);
        setText("");
      }}
    >
      <SearchArea>
        <Input
          type={"text"}
          placeholder={"Nova Busca"}
          value={text}
          onChange={(evt) => setText(evt.target.value)}
        />
        <SearchButton type={"submit"} disabled={!isAble}>
          Buscar
        </SearchButton>
      </SearchArea>
      <AlertArea>
        {!isAble && (
          <span> * A pesquisa deve conter entre 4 e 32 caracteres. </span>
        )}
      </AlertArea>
    </Container>
  );
};

export default SearchBox;
