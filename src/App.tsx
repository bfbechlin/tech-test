import React, { useEffect } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";

import SearchBox from "./components/SearchBox";
import InspectionItem from "./components/InspectionItem";
import AlertCard from "./components/AlertCard";

import {
  deleteInspection,
  createAndGetInspection,
  getPersistedInspections,
  getInspection,
} from "./store/inspection/";
import { deleteAlert } from "./store/alert/";
import { RootState } from "./store/";

const Container = styled.div`
  width: 100%;
  min-height: 100vh;
`;

const Content = styled.div`
  margin-left: auto;
  margin-right: auto;

  width: 800px;
  @media (max-width: 768px) {
    width: 600px;
  }
  @media (max-width: 576px) {
    width: 400px;
  }

  padding-top: ${(props) => props.theme.spacing[6]};
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Title = styled.h1`
  color: ${(props) => props.theme.colors.green[500]};
  display: inline-block;
`;

const App = () => {
  const dispatch = useDispatch();
  const alerts = useSelector((state: RootState) => state.alert);
  const inspections = useSelector(
    (state: RootState) => state.inspection.entities
  );
  useEffect(() => {
    dispatch(getPersistedInspections());
  }, []);
  return (
    <Container>
      <Content>
        <Title> INSPECIONADOR </Title>
        {alerts.map((alert, i) => (
          <AlertCard
            key={i}
            timestamp={alert.timestamp}
            type={alert.type}
            message={alert.message}
            onClose={(id: number) => {
              dispatch(deleteAlert(id));
            }}
          />
        ))}
        <SearchBox
          onSubmit={(keyword: string) => {
            dispatch(createAndGetInspection(keyword));
          }}
        />
        <h2> Inspeções solicitadas </h2>
        <p>
          Clique sobre o ID da inspeção para mostrar os resultados. Caso a
          inspeção ainda esteja em andamento, use o botão de recarregar para
          atualizar para os resultados mais recentes.
        </p>
        {Object.values(inspections).map(({ id, status, urls }) => (
          <InspectionItem
            key={id}
            id={id}
            status={status}
            urls={urls}
            onRefresh={() => dispatch(getInspection(id))}
            onDelete={() => dispatch(deleteInspection(id))}
          />
        ))}
      </Content>
    </Container>
  );
};

export default App;
