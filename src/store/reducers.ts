import { combineReducers } from "redux";
import inpectionReducer from "./inspection";
import alertsReducer from "./alert";

const rootReducer = combineReducers({
  inspection: inpectionReducer,
  alert: alertsReducer,
});

export default rootReducer;
