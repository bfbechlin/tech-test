import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "../http";
import { Inspection } from "./types";
import { INSPECTION_PERSISTANT_KEY } from "./types";
import { getValue } from "../../utils/persist";

export const getInspection = createAsyncThunk(
  "inspection/get",
  async (id: string, { rejectWithValue }) => {
    try {
      const response = await axios.get(`/crawl/${id}`);
      return response.data as Inspection;
    } catch {
      return rejectWithValue(id);
    }
  }
);

export const createAndGetInspection = createAsyncThunk(
  "inspection/create",
  async (keyword: string, { dispatch, rejectWithValue }) => {
    try {
      const response = await axios.post("/crawl", { keyword });
      const id = response.data.id as string;
      if (!id) {
        throw new Error("Invalid Response");
      }
      dispatch(actions.getInspection(id));
      return { id, keyword };
    } catch {
      return rejectWithValue(keyword);
    }
  }
);

export const getPersistedInspections = createAsyncThunk(
  "inspection/get-persisted",
  async (_, { dispatch }) => {
    const ids = getValue(INSPECTION_PERSISTANT_KEY, []) as string[];
    ids.forEach((id) => dispatch(actions.getInspection(id)));
    return ids;
  }
);

export const actions = {
  getInspection,
  createAndGetInspection,
  getPersistedInspections,
};
