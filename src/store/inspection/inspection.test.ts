import reducer, { deleteInspection } from "./index";
import { INSPECTION_PERSISTANT_KEY } from "./types";
import {
  active_inspection,
  done_inspection,
} from "../../__tests__/inspection.mock";
import {
  createAndGetInspection,
  getInspection,
  getPersistedInspections,
  actions,
} from "./actions";
import { setValue, getValue } from "../../utils/persist";
import http from "../http";

jest.mock("../../utils/persist", () => ({
  setValue: jest.fn(),
  getValue: jest.fn(() => [active_inspection.id, done_inspection.id]),
}));

jest.mock("../http");
const axios = http as jest.Mocked<typeof http>;

describe("Inspection-Slice", () => {
  afterAll(() => {
    jest.unmock("../../utils/persist");
    jest.unmock("../http");
  });

  describe("reducers", () => {
    it("should initilize state", () => {
      const nextState = reducer(undefined, { type: undefined });
      expect(nextState).toEqual({ ids: [], entities: {} });
    });

    it("should delete the inspection by id", () => {
      const state = {
        ids: [active_inspection.id, done_inspection.id],
        entities: {
          [active_inspection.id]: active_inspection,
          [done_inspection.id]: done_inspection,
        },
      };
      const nextState = reducer(state, deleteInspection(active_inspection.id));
      expect(nextState).toEqual({
        ids: [done_inspection.id],
        entities: { [done_inspection.id]: done_inspection },
      });
    });
  });

  describe("extra reducers", () => {
    it("should add the inspection id in the ids list and save in storage when createAndGet is fullfilled", () => {
      const nextState = reducer(
        { ids: [], entities: {} },
        createAndGetInspection.fulfilled(
          { id: active_inspection.id, keyword: "TEST" },
          "",
          ""
        )
      );
      expect(nextState).toEqual({ ids: [active_inspection.id], entities: {} });
      expect(setValue).toHaveBeenCalledWith(INSPECTION_PERSISTANT_KEY, [
        active_inspection.id,
      ]);
    });

    it("should add the inspection to the entities map when GetInspection is fullfilled", () => {
      const nextState = reducer(
        { ids: [active_inspection.id], entities: {} },
        getInspection.fulfilled(active_inspection, "", "")
      );
      expect(nextState).toEqual({
        ids: [active_inspection.id],
        entities: {
          [active_inspection.id]: active_inspection,
        },
      });
    });

    it("should add the ids to the id list when getPersisted is fullfilled ", () => {
      const nextState = reducer(
        { ids: [], entities: {} },
        getPersistedInspections.fulfilled([active_inspection.id], "")
      );
      expect(nextState).toEqual({ ids: [active_inspection.id], entities: {} });
    });
  });

  describe("thunk actions", () => {
    let dispatch;

    beforeEach(() => {
      dispatch = jest.fn();
    });

    describe("getInspection", () => {
      it("should call the api using the inspection id returning the inspection when successfull", async () => {
        axios.get.mockResolvedValue({ data: active_inspection });

        const action = getInspection(active_inspection.id);
        const { payload: inspection } = await action(dispatch, () => {}, {});

        expect(inspection).toEqual(active_inspection);
        expect(axios.get).toHaveBeenCalledWith(
          `/crawl/${active_inspection.id}`
        );
      });

      it("should reject something unexpected happen", async () => {
        axios.get.mockRejectedValue("ERROR");

        const action = getInspection(active_inspection.id);
        const { payload: id } = await action(dispatch, () => {}, {});

        expect(id).toEqual(active_inspection.id);
        expect(axios.get).toHaveBeenCalledWith(
          `/crawl/${active_inspection.id}`
        );
      });
    });

    describe("createAndGetInspection", () => {
      it("should call the api with the keyword if successfully registered should call then should call the getInspection", async () => {
        // mocking getInspection
        actions.getInspection = jest.fn() as any;
        axios.post.mockResolvedValue({ data: { id: active_inspection.id } });

        const keyword = "TEST";
        const action = createAndGetInspection(keyword);
        const { payload } = await action(dispatch, () => {}, {});

        expect(payload).toEqual({ id: active_inspection.id, keyword });
        expect(axios.post).toHaveBeenCalledWith("/crawl", { keyword });
        expect(actions.getInspection).toHaveBeenCalledWith(
          active_inspection.id
        );
      });

      it("should call the api with the keyword if id is not part of the response then reject and not call getInspection", async () => {
        // mocking getInspection
        actions.getInspection = jest.fn() as any;
        axios.post.mockResolvedValue({ data: { status: 404 } });

        const keyword = "TEST";
        const action = createAndGetInspection(keyword);
        const { payload } = await action(dispatch, () => {}, {});

        expect(payload).toEqual(keyword);
        expect(axios.post).toHaveBeenCalledWith("/crawl", { keyword });
        expect(actions.getInspection).toHaveBeenCalledTimes(0);
      });

      it("should call the api with the keyword if something wrong happen then reject and not call getInspection", async () => {
        // mocking getInspection
        actions.getInspection = jest.fn() as any;
        axios.post.mockRejectedValue("ERROR");

        const keyword = "TEST";
        const action = createAndGetInspection(keyword);
        const { payload } = await action(dispatch, () => {}, {});

        expect(payload).toEqual(keyword);
        expect(axios.post).toHaveBeenCalledWith("/crawl", { keyword });
        expect(actions.getInspection).toHaveBeenCalledTimes(0);
      });
    });

    describe("getPersistedInspections", () => {
      it("should call the storage looking for persisted ids and also call the getInspection thunk for each one", async () => {
        // mocking getInspection
        actions.getInspection = jest.fn() as any;

        const action = getPersistedInspections();
        const { payload: ids } = await action(dispatch, () => {}, {});

        expect(getValue).toHaveBeenCalledWith(INSPECTION_PERSISTANT_KEY, []);
        expect(ids).toEqual([active_inspection.id, done_inspection.id]);

        expect(dispatch).toHaveBeenCalledTimes(2 * 2);
        expect(actions.getInspection).toHaveBeenCalledTimes(2);
        expect(actions.getInspection).toHaveBeenNthCalledWith(
          1,
          active_inspection.id
        );
        expect(actions.getInspection).toHaveBeenNthCalledWith(
          2,
          done_inspection.id
        );
      });
    });
  });
});
