import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { INSPECTION_PERSISTANT_KEY } from "./types";
import { Inspection } from "./types";
import {
  createAndGetInspection,
  getInspection,
  getPersistedInspections,
} from "./actions";
import { setValue } from "../../utils/persist";

export interface InspectionEntities {
  [key: string]: Inspection;
}

export interface InspectionState {
  ids: string[];
  entities: InspectionEntities;
}

export const inspectionSlice = createSlice({
  name: "inspections",
  initialState: { ids: [], entities: {} } as InspectionState,
  reducers: {
    deleteInspection: (state, action: PayloadAction<string>) => {
      const id = action.payload;
      const ids = state.ids.filter((_id) => id !== _id);

      setValue(INSPECTION_PERSISTANT_KEY, ids);

      const entities = { ...state.entities };
      delete entities[id];
      return { ids, entities };
    },
  },
  extraReducers: (builder) => {
    builder.addCase(
      createAndGetInspection.fulfilled,
      ({ ids: _ids, entities }, { payload: { id } }) => {
        const ids = [..._ids, id];
        setValue(INSPECTION_PERSISTANT_KEY, ids);
        return { ids, entities };
      }
    );
    builder.addCase(
      getInspection.fulfilled,
      ({ ids, entities: _entities }, { payload: entity }) => {
        const entities = { ..._entities, [entity.id]: entity };
        return { ids, entities };
      }
    );
    builder.addCase(
      getPersistedInspections.fulfilled,
      ({ ids: _ids, entities }, { payload: ids }) => {
        return { ids: [..._ids, ...ids], entities };
      }
    );
  },
});

export default inspectionSlice;
