import inspectionSlice from "./slices";

export const { deleteInspection } = inspectionSlice.actions;

export {
  createAndGetInspection,
  getInspection,
  getPersistedInspections,
} from "./actions";

export default inspectionSlice.reducer;
