export const INSPECTION_PERSISTANT_KEY = "inspections";

export enum InspectionStatus {
  DONE = "done",
  ACTIVE = "active",
}

export interface Inspection {
  id: string;
  urls: string[];
  status: InspectionStatus;
}
