import axios from "axios";

export const http = axios.create({
  baseURL: "http://testapp.axreng.com:3000/",
});

export default http;
