import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Alert, AlertType } from "./types";
import { createAndGetInspection, getInspection } from "../inspection/actions";

export const alertSlice = createSlice({
  name: "alert",
  initialState: [] as Alert[],
  reducers: {
    deleteAlert: (state, action: PayloadAction<number>) => {
      const { payload: timestamp } = action;
      return state.filter((alert) => alert.timestamp !== timestamp);
    },
  },
  extraReducers: (builder) => {
    builder.addCase(
      createAndGetInspection.fulfilled,
      (state, { payload: { id, keyword } }) => {
        const alert = {
          timestamp: Date.now(),
          type: AlertType.success,
          message: `A inspeção pela palavra ${keyword} for cadastrada com sucesso pelo ID ${id}`,
        };
        return [...state, alert];
      }
    );
    builder.addCase(createAndGetInspection.rejected, (state, { payload }) => {
      const alert = {
        timestamp: Date.now(),
        type: AlertType.error,
        message: `Ocorreu um erro no cadastro da inspeção por ${payload}. Tente novamente e caso o problema persista entre em contato.`,
      };
      return [...state, alert];
    });
    builder.addCase(getInspection.rejected, (state, { payload }) => {
      const alert = {
        timestamp: Date.now(),
        type: AlertType.error,
        message: `Ocorreu um erro na consulta da inspeção com ID ${payload}. Tente novamente e caso o problema persista entre em contato.`,
      };
      return [...state, alert];
    });
  },
});

export default alertSlice;
