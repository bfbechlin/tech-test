import alertSlice from "./slices";

export const { deleteAlert } = alertSlice.actions;

export default alertSlice.reducer;
