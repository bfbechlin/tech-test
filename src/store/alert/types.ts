export enum AlertType {
  error,
  success,
}

export interface Alert {
  timestamp: number;
  type: AlertType;
  message: string;
}
