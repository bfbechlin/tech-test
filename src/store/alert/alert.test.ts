import reducer, { deleteAlert } from "./index";
import { errorAlert, successAlert } from "../../__tests__/alert.mock";
import { createAndGetInspection, getInspection } from "../inspection";

describe("Alert-Slice", () => {
  describe("reducers", () => {
    it("should initilize state", () => {
      const nextState = reducer(undefined, { type: undefined });
      expect(nextState).toHaveLength(0);
    });

    it("should initilize state", () => {
      const nextState = reducer(
        [errorAlert, successAlert],
        deleteAlert(successAlert.timestamp)
      );
      expect(nextState).toHaveLength(1);
    });
  });

  describe("extra reducers", () => {
    it("should add an success alert when createAndGetInspection is fullfilled", () => {
      const id = "00";
      const keyword = "test";
      const nextState = reducer(
        [],
        createAndGetInspection.fulfilled({ id, keyword }, "", "")
      );
      expect(nextState).toHaveLength(1);
      const alert = nextState[0];
      expect(alert.message).toBe(
        `A inspeção pela palavra ${keyword} for cadastrada com sucesso pelo ID ${id}`
      );
      expect(alert.type).toBe(successAlert.type);
    });

    it("should add an error alert when createAndGetInspection is rejected", () => {
      const payload = "test";
      const nextState = reducer(
        [],
        createAndGetInspection.rejected(new Error(payload), payload, payload)
      );
      expect(nextState).toHaveLength(1);
      const alert = nextState[0];
      expect(alert.message).toMatch(
        /Ocorreu um erro no cadastro da inspeção por/
      );
      expect(alert.type).toBe(errorAlert.type);
    });

    it("should add an error alert when getInspection is rejected", () => {
      const payload = "test";
      const nextState = reducer(
        [],
        getInspection.rejected(new Error(payload), payload, payload)
      );
      expect(nextState).toHaveLength(1);
      const alert = nextState[0];
      expect(alert.message).toMatch(
        /Ocorreu um erro na consulta da inspeção com ID/
      );
      expect(alert.type).toBe(errorAlert.type);
    });
  });
});
