import React from "react";
import * as reactRedux from "react-redux";
import { render, fireEvent, cleanup } from "./__tests__";
import { successAlert } from "./__tests__/alert.mock";
import { active_inspection } from "./__tests__/inspection.mock";
import App from "./App";

import {
  getInspection,
  createAndGetInspection,
  getPersistedInspections,
} from "./store/inspection/actions";
import inspectionSlice from "./store/inspection/slices";
import alertSlice from "./store/alert/slices";

jest.mock("./store/inspection/actions");

describe("App", () => {
  afterEach(cleanup);

  const state = {
    alert: [successAlert],
    inspection: {
      ids: [active_inspection.id],
      entities: { [active_inspection.id]: active_inspection },
    },
  };
  const dispatch = jest.fn();
  jest.spyOn(reactRedux, "useDispatch").mockReturnValue(dispatch);
  jest
    .spyOn(reactRedux, "useSelector")
    .mockImplementation((selector) => selector(state));

  it("should get the persisted inspection once mounted", () => {
    render(<App />);

    expect(getPersistedInspections).toHaveBeenCalled();
    expect(dispatch).toHaveBeenCalledWith(getPersistedInspections());
  });

  it("should call the createAndGetInspection when entering a valid keyword and clicking the search button", () => {
    const keyword = "test";
    const { getByPlaceholderText, getByText } = render(<App />);
    const input = getByPlaceholderText("Nova Busca");
    const button = getByText("Buscar");

    fireEvent.change(input, { target: { value: keyword } });
    fireEvent.click(button);

    expect(createAndGetInspection).toHaveBeenCalledWith(keyword);
    expect(dispatch).toHaveBeenCalledWith(createAndGetInspection(keyword));
  });

  it("should call getInspection when trying to refresh an inspection", () => {
    const { getByText } = render(<App />);
    const button = getByText("Recarregar");

    fireEvent.click(button);

    expect(getInspection).toHaveBeenCalledWith(active_inspection.id);
    expect(dispatch).toHaveBeenCalledWith(getInspection(active_inspection.id));
  });

  it("should call deleteInspection when trying to delete an inspection", () => {
    const { deleteInspection } = inspectionSlice.actions;
    const { getByText } = render(<App />);
    const button = getByText("Deletar");

    fireEvent.click(button);

    expect(dispatch).toHaveBeenCalledWith(
      deleteInspection(active_inspection.id)
    );
  });

  it("should call deleteAlert when trying to delete an alert", () => {
    const { deleteAlert } = alertSlice.actions;
    const { getByTitle } = render(<App />);
    const button = getByTitle("Fechar");

    fireEvent.click(button);

    expect(dispatch).toHaveBeenCalledWith(deleteAlert(successAlert.timestamp));
  });
});
