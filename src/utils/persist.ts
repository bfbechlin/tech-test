const ls = global.localStorage;

export const getValue = (key: string, _default: Object) => {
  try {
    const item = ls.getItem(key);
    if (item) {
      return JSON.parse(item);
    }
  } catch {}
  return _default;
};

export const setValue = (key: string, item: Object) => {
  try {
    ls.setItem(key, JSON.stringify(item));
  } catch {}
};
