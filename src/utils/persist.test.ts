import { getValue, setValue } from "./persist";

describe("Persist", () => {
  describe("getValue", () => {
    it("it should call the local storage get item and parse the response", () => {
      const key = "TEST";
      const items = ["test1", "test2"];
      Storage.prototype.getItem = jest.fn((_) => JSON.stringify(items));

      expect(getValue(key, [])).toEqual(items);
      expect(global.localStorage.getItem).toHaveBeenCalledWith(key);
    });

    it("it should return the default if not found or any parsing error happen", () => {
      const key = "TEST";
      Storage.prototype.getItem = jest.fn((_) => "");

      expect(getValue(key, [])).toEqual([]);
      expect(global.localStorage.getItem).toHaveBeenCalledWith(key);
    });
  });

  describe("setValue", () => {
    it("it should call the local storage set item and stringify", () => {
      const key = "TEST";
      const items = ["test1", "test2"];
      Storage.prototype.setItem = jest.fn(() => {});

      setValue(key, items);
      expect(global.localStorage.setItem).toHaveBeenCalledWith(
        key,
        JSON.stringify(items)
      );
    });

    it("it should not throw any error when the object is not valid", () => {
      const key = "TEST";
      const items = ["test1", "test2"];
      Storage.prototype.setItem = jest.fn(() => {
        throw new Error("TEST");
      });

      setValue(key, items);
      expect(global.localStorage.setItem).toHaveBeenCalledWith(
        key,
        JSON.stringify(items)
      );
    });
  });
});
