import baseStyled, {
  ThemedStyledInterface,
  createGlobalStyle,
} from "styled-components";

export const theme = {
  spacing: {
    0: "0",
    0.5: "0.125rem",
    1: "0.25rem",
    1.5: "0.375rem",
    2: "0.5rem",
    2.5: "0.625rem",
    3: "0.75rem",
    3.5: "0.875rem",
    4: "1rem",
    5: "1.25rem",
    6: "1.5rem",
    7: "1.75rem",
    8: "2rem",
    9: "2.25rem",
    10: "2.5rem",
  },
  colors: {
    green: {
      50: "#ECFDF5",
      100: "#D1FAE5",
      200: "#A7F3D0",
      300: "#6EE7B7",
      400: "#34D399",
      500: "#10B981",
      600: "#059669",
      700: "#047857",
      800: "#065F46",
      900: "#064E3B",
    },
    red: {
      50: "#FEF2F2",
      100: "#FEE2E2",
      200: "#FECACA",
      300: "#FCA5A5",
      400: "#F87171",
      500: "#EF4444",
      600: "#DC2626",
      700: "#B91C1C",
      800: "#991B1B",
      900: "#7F1D1D",
    },
  },
  radius: {
    none: "0",
    sm: ".125rem",
    md: ".25rem",
    lg: ".5rem",
    full: "9999px",
  },
  shadow: {
    sm: "0 .25rem 0.5rem #00000026",
    md: "0 .5rem 1rem #00000026",
    lg: "0 1rem 2rem #00000026",
  },
};

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    background-color: rgba(243, 244, 246, 1);
    
    font-family: 'Graphik', 'Roboto', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Oxygen',
      'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
      sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  * {
    border-width: 0px;
  }
`;

export default GlobalStyle;

export type Theme = typeof theme;
export const styled = baseStyled as ThemedStyledInterface<Theme>;
