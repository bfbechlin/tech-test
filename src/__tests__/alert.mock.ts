import { Alert, AlertType } from "../store/alert/types";

export const successAlert: Alert = {
  timestamp: 0,
  message: "Test success message",
  type: AlertType.success,
};

export const errorAlert: Alert = {
  timestamp: 1,
  message: "Test error message",
  type: AlertType.error,
};
