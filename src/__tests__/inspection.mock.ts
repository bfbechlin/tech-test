import { Inspection, InspectionStatus } from "../store/inspection/types";

export const done_inspection: Inspection = {
  id: "id-1",
  urls: ["link1", "link2"],
  status: InspectionStatus.DONE,
};

export const active_inspection: Inspection = {
  id: "id-2",
  urls: ["link1", "link2"],
  status: InspectionStatus.ACTIVE,
};

export const four_urls_inspection: Inspection = {
  id: "id-3",
  urls: ["link1", "link2", "link3", "link4", "link5"],
  status: InspectionStatus.ACTIVE,
};

export const eight_urls_inspection: Inspection = {
  id: "id-4",
  urls: [
    "link1",
    "link2",
    "link3",
    "link4",
    "link5",
    "link6",
    "link7",
    "link8",
  ],
  status: InspectionStatus.ACTIVE,
};

export default active_inspection;
